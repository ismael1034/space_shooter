﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyGameManager : MonoBehaviour {

    public int highscore;
    public Text livesText;
    public Text highscoreText;
    public int lives;

    private static MyGameManager instance;

    void Awake()
    {

        if (instance == null)
        {
            instance = this;
        }

    }

        public static MyGameManager getInstance()
    {
        return instance;
    }



    // Use this for initialization
    void Start () {

        lives = 3;
        highscore = 0;
        highscoreText.text = 0.ToString("D5");
      //  lives.text = "x3";
       
        livesText.text = "x " + lives.ToString();

	}
	
	// Update is called once per frame
    public void AddHighscore(int value)
    {
        highscore += value;
        highscoreText.text = ++;

    }

	public void LoseLive () {

        lives--;
        livesText.text = "x " + lives.ToString();
	}
}
