﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorBig : MeteorTiny {


    // Use this for initialization
    protected override void Explode() {
        MeteorManager.getInstance().LaunchMeteor(2, transform.position, new Vector2(-4, -4), -5);
        MeteorManager.getInstance().LaunchMeteor(2, transform.position, new Vector2(4, -4), 5);

        base.Explode();
    }
	
	

}
