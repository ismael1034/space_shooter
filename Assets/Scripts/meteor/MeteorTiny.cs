﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorTiny : Meteor
{

    public GameObject[] graphics;
    // Use this for initialization
    void Awake()
    {

        int selected = Random.Range(0, graphics.Length);
        for(int i = 0; i < graphics.Length; i++)
        {
            if(i != selected)
            {
                graphics[i].SetActive(false);
            }
        }
        enabled = true;
    }



}
