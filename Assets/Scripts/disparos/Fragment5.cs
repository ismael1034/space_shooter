﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fragment5 : Bullet
{

    public int maxAmmo = 5;
    Cartridge cartridge;
    public int timeToExplode = 0;
    private float currentTime = 0f;

    void Awake()
    {
        cartridge = GameObject.Find("Weapons").GetComponent<Weapons>().cartridges[0];
    }

    protected new virtual void Update()
    {
        base.Update();
        if (shooting)
        {
            currentTime += Time.deltaTime;
            if (currentTime > timeToExplode)
            {
                ShotMiniBullets();
            }
        }
        else
        {
            currentTime = 0f;
        }
    }

    void ShotMiniBullets()
    {
      
        float z = transform.rotation.eulerAngles.z;
        cartridge.GetBullet().Shot(transform.position, 0 + z);
        cartridge.GetBullet().Shot(transform.position, 20 + z);
        cartridge.GetBullet().Shot(transform.position, -20 + z);
        cartridge.GetBullet().Shot(transform.position, 40 + z);
        cartridge.GetBullet().Shot(transform.position, -40 + z);

        Reset();
    }
}
