﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragmentBullets2 : Bullet
{

    public int maxAmmo = 3;
    Cartridge cartridges;
    public int timeToExplode = 0;
    private float currentTime = 0f;

    void Awake()
    {
        cartridges = GameObject.Find("Weapons").GetComponent<Weapons>().cartridges[0];
    }

    protected new virtual void Update()
    {
        base.Update();
        if (shooting)
        {
            currentTime += Time.deltaTime;
            if (currentTime >= timeToExplode)
            {
                ShotBullethree();
            }
        }
       


     
    }

    void ShotBullethree()
    {
        float z = transform.rotation.eulerAngles.z;
        cartridges.GetBullet().Shot(transform.position, 0);
        cartridges.GetBullet().Shot(transform.position, -45 + z);
        cartridges.GetBullet().Shot(transform.position, 45 + z);
        Reset();
    }
}
